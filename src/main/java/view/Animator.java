package view;



public interface Animator {
	
	
	
	/**
	 * Stop the Animator loop
	 */
	public void stop();

	/**
	 * Call handle method
	 */
	public void start();

}
